# SFCs are overrated

A dead simple, pipe-in pipe-out build tool that takes an object-style vue component with a template field and spits out an object-style vue component with a render field. Very specialized for my use case, so will probably take some tweaking.

## Setup
I'm not including a package.json, but the following packages are required:

`npm i -D @vue/component-compiler-utils vue-template-compiler ast-types recast acorn`

## Usage
`cat component.js | node build.js > component.runtime.js`

This build tool makes two simple assumptions about how your vue component files are formatted. The component must be an object definition, and the template string should be a tagged template literal using a function named `html`. A basic structure would look like the following:

```javascript
const html = arg => arg.join(''); // no-op used for identifying the target template literal

// Do whatever with the object
// Assign it to a variable, export default, set as module.exports, use directly in `components: {}`, etc.
export default {
    // This must be a tagged template literal using the 'html' function.
    template: html`
        <div>Your template code here</div>
    `
}
```

This outputs the following code block.
It's fairly verbose but my use case is accomodating strict CSP policies for AMO so filesize wasn't an issue.
```javascript
const html = arg => arg.join(''); // no-op used for identifying the target template literal

// Do whatever with the object
// Assign it to a variable, export default, set as module.exports, use directly in `components: {}`, etc.
export default {
    render: (() => {
    var render = function() {
      var _vm = this
      var _h = _vm.$createElement
      var _c = _vm._self._c || _h
      return _c("div", [_vm._v("Your template code here")])
    }
    var staticRenderFns = []
    render._withStripped = true
    ;
    return render;
    })(),

    staticRenderFns: (() => {
    var render = function() {
      var _vm = this
      var _h = _vm.$createElement
      var _c = _vm._self._c || _h
      return _c("div", [_vm._v("Your template code here")])
    }
    var staticRenderFns = []
    render._withStripped = true
    ;
    return staticRenderFns;
    })()
};
```

## More about my use case
I have [an addon](https://gitlab.com/UniQMG/tetrio-plus) with a hundred or so users using Vue that just got taken down for having `unsafe-eval` in the CSP. It uses some other libraries that require it, but my current goal is the elimination of the `eval` dependencies in Vue. Unfortunately, my addon has 10 seperate internal pages which would be annoying to webpackerize. Furthermore, I harbor an incomphrehensible hate of build tools in personal projects. Thus, I had two goals: No editing of existing source code and all existing code should work in dev without being built.

My existing deploy step for the project was a quick bash script that filtered and zipped files relevent to the firefox addon, so adding a transformer script would be easy. I call it a deploy step because it's completely irrelevent to the actual functionality of the code and only exists to keep filesizes down and the AMO validator happy.
